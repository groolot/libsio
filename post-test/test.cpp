#include "libsio.h"
#include <string>
#include <iostream>
int main()
{
  char c;
  short int si;
  int i;
  long int l;
  float f;
  double d;
  std::string s;
  sio::saisir_valeur("char", c, '1', 'Z');
  std::cout << "La valeur saisie est : '" << c << "'" << std::endl;
  sio::saisir_valeur("short int", si);
  std::cout << "La valeur saisie est : " << si << std::endl;
  sio::saisir_valeur("int", i);
  std::cout << "La valeur saisie est : " << i << std::endl;
  sio::saisir_valeur("long int", l);
  std::cout << "La valeur saisie est : " << l << std::endl;
  sio::saisir_valeur("float", f);
  std::cout << "La valeur saisie est : " << f << std::endl;
  sio::saisir_valeur("double", d);
  std::cout << "La valeur saisie est : " << d << std::endl;
  sio::saisir_valeur("string", s);
  std::cout << "La valeur saisie est : \"" << s << "\"" << std::endl;
  return 0;
}
