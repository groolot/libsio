/*!\file libsio.h
 * \mainpage Programmer avec la bibliothèque libsio
 * \author Grégory DAVID <gregory.david@ac-nantes.fr>
 *
 * \htmlonly
 * <a href="https://framagit.org/sio/libsio/commits/master"><img alt="pipeline status" src="https://framagit.org/sio/libsio/badges/master/pipeline.svg" /></a>
 * \endhtmlonly
 *
 * Ensemble des fonctions facilitatrices à l'apprentissage de la
 * programmation.
 *
 * Cette bibliothèque est utilisée dans le cadre de l'enseignement des
 * bases de la programmation en STS1 du BTS SIO.
 *
 * La documentation générale de la bibliothèque \p **libsio** est
 * disponible dans le fichier \ref libsio.h
 *
 * \par Comment faire ?
 * \li \subpage Installation
 * \li \subpage Utilisation
 *
 *
 *
 *
 *
 * \page Installation Installer la libsio sur son système
 * \li \subpage Debian
 * \li \subpage AutresGNU
 *
 *
 *
 *
 *
 * \page Debian Debian GNU/Linux
 * \warning Doit être réalisé avec les droits du **super-utilisateur**
 *
 * \section GestionAPT Configuration APT
 * \subsection APTKey Ajout de la clef GPG de signature des paquets
 * \code{.sh}
 * wget http://www.bts-malraux72.net/~g.david/sio.asc -O - | apt-key add -
 * \endcode
 *
 * \subsection APTSources Ajout du dépôt officiel des paquets
 * \code{.sh}
 * cat > /etc/apt/sources.list.d/sio.list <<EOF
 * deb http://www.bts-malraux72.net/~g.david/debian/ stable/
 * deb-src http://www.bts-malraux72.net/~g.david/debian/ stable/
 * EOF
 * \endcode
 *
 * \section InstallUpgrade Installation et Mise à jour des paquets
 * \subsection APTUpdateInstall Installation des paquets
 * \code{.sh}
 * apt update && apt install libsio-dev libsio1
 * \endcode
 *
 * \subsection APTUpgrade Mise à jour des paquets
 * \code{.sh}
 * apt update && apt upgrade libsio-dev libsio1
 * \endcode
 *
 *
 * \page AutresGNU Autres GNU/Linux
 * \section PrerequisGNU Prérequis
 * \li \p unzip >= 6.0
 * \li \p GNU tar >= 1.26
 * \li \p g++ >= 6.3
 * \li \p GNU make >= 4.1
 *
 * \section ExempleGNU Exemple
 * \subsection TelechargementGNU Téléchargement
 * Télécharger la dernière version (le \p tag nommé \e latest est
 * toujours la version la plus élevée) de distribution du \p tarball
 * depuis l'espace projet :
 * https://framagit.org/sio/libsio/-/jobs/artifacts/latest/download?job=dist:tarball
 * \code{.sh}
 * wget "https://framagit.org/sio/libsio/-/jobs/artifacts/latest/download?job=dist:tarball" -O artifacts.zip
 * unzip artifacts.zip
 * tar xf libsio-*.tar.gz
 * cd libsio-*
 * \endcode
 *
 * \subsection Make Construction logicielle
 * Construire à l'aide des outils mis à disposition dans le paquet de
 * distribution :
 * \code{.sh}
 * ./configure
 * make
 * \endcode
 *
 * \subsection InstallationGNU Installation
 * Installer en suivant la procédure habituelle des \p
 * autotools.
 * \code{.sh}
 * make install
 * \endcode
 * \warning Installation avec les droits du **super-utilisateur**
 *
 *
 *
 *
 *
 * \page Utilisation Utiliser la libsio dans son programme
 *
 * L'utilisateur de la bilbiothèque doit s'assurer qu'elle est bien
 * installée sur son système : voir \subpage Installation
 *
 * Il suffit ensuite de fournir les éléments de configuration de la
 * compilation et de l'édition des liens au compilateur.
 *
 * Vous pouvez utiliser \p pkg-config pour obtenir les éléments de
 * configuration adaptés à votre installation.
 *
 * \par Programme simple
 *
 * \code{.cpp}
 * // Fichier main.cpp
 * #include <libsio.h>
 * #include <iostream>
 *
 * int main(int argc, char **argv){
 *   unsigned int valeur_aleatoire;
 *   valeur_aleatoire = sio::aleatoire(10, 1024);
 *   std::cout << "La valeur aléatoire est : "
 *             << valeur_aleatoire << std::endl;
 *   return 0;
 * }
 * \endcode
 *
 * \par Compilation
 *
 * \code{.sh}
 * g++ -c -o main.o main.c
 * \endcode
 *
 * \par Édition des liens
 *
 * \code{.sh}
 * g++ -lsio -o main main.o
 * \endcode
 *
 * \par Vérification que l'exécutable est bien lié à la libsio
 *
 * \code{.sh}
 * ldd main
 *    linux-vdso.so.1 (0x00007ffd30fdd000)
 *    libsio.so.1 => /lib/x86_64-linux-gnu/libsio.so.1 (0x00007f42cc5d8000)
 *    libstdc++.so.6 => /lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007f42cc455000)
 *    libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f42cc2c1000)
 *    libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f42cc2a7000)
 *    libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f42cc0ea000)
 *    /lib64/ld-linux-x86-64.so.2 (0x00007f42cc811000)
 * \endcode
 *
 */
#ifndef LIBSIO_H
#define LIBSIO_H

#include <string>

/*!\namespace sio
 *
 * L'espace de nom **sio** est l'espace de nom spécifique de
 * ce que l'équipe pédagogique met à disposition
 */
namespace sio {

/*!\brief Fonction génératrice de valeurs pseudo-aléatoires
 *
 * Cette fonction permet d'obtenir une valeur pseudo-aléatoire
 * comprise entre les deux bornes fournies en paramètres.
 *
 * \param min Défini la borne minimum possible, valeur incluse
 * \param max Défini la borne maximum possible, valeur incluse
 * \return
 * \li La valeur de \p min si \p min et \p max sont égaux
 * \li Un nombre entier non signé compris entre la valeur \p min et la valeur \p max dans tous les autres cas
 *
 * \note Si \p min est supérieur à \p max, alors les valeurs de \p min
 * et \p max sont échangées pour considérer les bornes comme inférieur
 * et supérieur convenablement.
 *
 * \par Exemples
 * \li Usages corrects
 * \code{.cpp}
 * unsigned int nombre_aleatoire;
 * nombre_aleatoire = aleatoire(0, 10);
 * nombre_aleatoire = aleatoire(8, 5);
 * \endcode
 *
 * \li Obtenir un nombre flottant simple précision (attention méthode médiocre)
 * \code{.cpp}
 * unsigned float nombre_aleatoire_flottant;
 * unsigned int maximum = 10000;
 * nombre_aleatoire_flottant = aleatoire(0, maximum) / (unsigned float)maximum
 * \endcode
 *
 * \warning Usages incorrects
 * \code{.cpp}
 * unsigned int nombre_aleatoire;
 * nombre_aleatoire = aleatoire(-5, 10);
 * nombre_aleatoire = aleatoire(3.14159, 4);
 * \endcode
 */
unsigned int aleatoire(unsigned int min, unsigned int max);

/*!\brief Modèle de fonctions permettant de demander une entrée utilisateur avec contrôle de saisie élémentaire
 *
 * L'utilisateur se verra offrir le \p message passé en paramètre et la valeur qu'il
 * saisira sera stockée dans le paramètre de \p sortie
 *
 * \tparam T Le type de la valeur de \p sortie attendue. Il faudra donc fournir en paramètre une variable
 * correctement typée à ce qui est attendu.
 *
 * \param message À afficher à l'utilisateur pour qu'il comprenne ce qu'on attend de lui
 * \param[out] sortie La variable en sortie, de type \p T, pour accueillir ce que l'utilisateur aura saisi
 *
 * \par Exemples
 * \li \p **char** : Voici comment demander à l'utilisateur de **saisir un caractère** imprimable de la table \p ASCII.
 * Si plusieurs caractères sont saisis par l'utilisateur, seul le premier est considéré.
 * \code{.cpp}
 * char caractere_saisi;
 * saisir_valeur("Quel caractère ?", &caractere_saisi);
 * std::cout << "Le caractère saisi est : "
 *           << caractere_saisi
 *           << std::endl;
 * \endcode
 *
 * \li \p **short int** : Voici comment demander à l'utilisateur de **saisir une petite valeur entière signée**.
 * \code{.cpp}
 * short int temperature;
 * saisir_valeur("Quelle est la température moyenne ?", &temperature);
 * std::cout << "La température saisie est : "
 *           << temperature
 *           << std::endl;
 * \endcode
 *
 * \li \p **int** : Voici comment demander à l'utilisateur de **saisir une valeur entière signée**.
 * \code{.cpp}
 * int circonference;
 * saisir_valeur("Quelle est la circonférence de la planète Terre ?", &circonference);
 * std::cout << "La circonférence saisie est : "
 *           << circonference
 *           << std::endl;
 * \endcode
 *
 * \li \p **long int** : Voici comment demander à l'utilisateur de **saisir une grande valeur entière signée**.
 * \code{.cpp}
 * long int distance;
 * saisir_valeur("Quelle est la distance (en km) entre la planète Terre et le Soleil ?", &distance);
 * std::cout << "La distance saisie est : "
 *           << distance
 *           << std::endl;
 * \endcode
 *
 * \li \p **float** : Voici comment demander à l'utilisateur de **saisir une valeur flottante simple précision signée** (codée sur 32 bits).
 * \code{.cpp}
 * float volume;
 * saisir_valeur("Quelle est le volume d'une pint (en Litre) ?", &volume);
 * std::cout << "Le volume saisi est : "
 *           << volume
 *           << std::endl;
 * \endcode
 *
 * \li \p **double** : Voici comment demander à l'utilisateur de **saisir une valeur flottante double précision signée** (codée sur 64 bits).
 * \code{.cpp}
 * double quantite;
 * saisir_valeur("Quelle est la quantité létale (en Gray) d'une irradiation pour un être humain ?", &quantite);
 * std::cout << "La dose létale d'irradiation a été définie à "
 *           << quantite
 *           << " Gy appliqué à un être humain"
 *           << std::endl;
 * \endcode
 *
 * \note Tous ces exemples possèdent leur contrepartie dans les types non signés (\p unsigned).
 * \li \p **unsigned double** : Voici comment demander à l'utilisateur de **saisir une valeur flottante double non signée**.
 * \code{.cpp}
 * unsigned double distance;
 * saisir_valeur("Quelle est la distance entre vos deux oreilles ?", &distance);
 * std::cout << "Vous venez de déclarer que "
 *           << distance
 *           << " centimètres séparaient vos deux oreilles"
 *           << std::endl;
 * \endcode
 */
template<typename T> void saisir_valeur(const char *message, T &sortie);

/*!\brief Modèle de fonctions permettant de demander une entrée utilisateur avec bornage et contrôle de saisie élémentaire
 *
 * Ce modèle de fonction L'utilisateur se verra offrir le \p message passé en paramètre,
 * agrémenté du bornage accepté pour la valeur demandée, et la valeur
 * qu'il saisira sera stockée dans le paramètre de \p sortie
 *
 * \note Tant que la saisie utilisateur ne correspond pas au type et limites
 * définies, la question est reposée à l'utilisateur
 *
 * \tparam T Le type de la valeur de \p sortie attendue. Il faudra donc fournir en paramètre une variable
 * correctement typée à ce qui est attendu.
 *
 * \param message À afficher à l'utilisateur pour qu'il comprenne ce qu'on attend de lui
 * \param[out] sortie La variable en sortie, de type \p T, pour accueillir ce que l'utilisateur aura saisi
 * \param min La borne inférieur acceptable inclusive
 * \param max La borne supérieur acceptable inclusive
 *
 * \par Exemples
 * Ne sont présentés ici que deux exemples. Vous pouvez vous inspirer de
 * ce qui a été décrit dans la version non bornée de la fonction
 * saisir_valeur()
 *
 * \li Voici comment demander à l'utilisateur de **saisir une valeur entière signée**, bornée **entre -172 et 3000**.
 * Si des caractères non numériques sont saisis par l'utilisateur, la fonction
 * redemande la saisie à l'utilisateur.
 * \code{.cpp}
 * int temperature;
 * saisir_valeur("Quelle est la température moyenne ?", &temperature, -172, 3000);
 * std::cout << "La température saisie est : "
 *           << temperature
 *           << std::endl;
 * \endcode
 * \li Voici comment demander à l'utilisateur de **saisir une valeur flottante double non signée**, bornée **entre 2.432 et 17.54321**.
 * Si des caractères non numériques sont saisis par l'utilisateur, la fonction
 * redemande la saisie à l'utilisateur.
 * \code{.cpp}
 * unsigned double distance;
 * unsigned double mon_minimum = 2.432;
 * unsigned double mon_maximum = 17.54321;
 * saisir_valeur("Quelle est la distance entre vos deux oreilles ?", &distance, mon_minimum, mon_maximum);
 * std::cout << "Vous venez de déclarer que "
 *           << distance
 *           << " centimètres séparaient vos deux oreilles"
 *           << std::endl;
 * \endcode
 */
template<typename T> void saisir_valeur(const char *message, T &sortie, T min, T max);

/*!\brief Implémentation spécifique de la fonction saisir_valeur()
 * pour assurer un questionnement binaire (deux choix)
 *
 * Cette fonction permet de poser une question à l'utilisateur, dont
 * la réponse est conditionnée entre deux choix (sortie \p bool).
 * Chacun des choix est identifié par un \p char donné en paramètre \p
 * yes et \p no.
 *
 * \param message À afficher à l'utilisateur pour qu'il comprenne ce qu'on attend de lui
 * \param[out] sortie La variable en sortie, de type \p bool, pour accueillir ce que l'utilisateur aura saisi
 * \param yes Si fourni, le caractère à afficher comme réponse positive à la question, sinon \p 'o' par défaut.
 * \param no Si fourni, le caractère à afficher comme réponse négative à la question, sinon \p 'n' par défaut.
 *
 * \par Exemples
 * \li Poser une question binaire à l'utilisateur et obtenir une réponse basée sur le choix entre \p 'o' et \p 'n' (Oui/Non).
 * \code{.cpp}
 * bool reponse;
 * saisir_valeur("Avez-vous pris une tasse de thé aujourd'hui ?", &reponse);
 * if(reponse == true){
 *   std::cout << "Vous venez de déclarer que vous aviez pris une tasse de thé aujourd'hui"
 *             << std::endl;
 * }
 * else{
 *   std::cout << "Vous venez de déclarer que vous n'aviez pas pris une tasse de thé aujourd'hui"
 *             << std::endl;
 * }
 * \endcode
 * \li Poser une question binaire à l'utilisateur et obtenir une réponse basée sur le choix entre \p 'V' et \p 'F' (Vrai/Faux).
 * \code{.cpp}
 * bool reponse;
 * saisir_valeur("À 20°C, l'eau est à l'état liquide ?", &reponse, 'V', 'F');
 * if(reponse == true){
 *   std::cout << "Bravo, vous avez correctement répondu"
 *             << std::endl;
 * }
 * else{
 *   std::cout << "Oh ! Regardez autour de vous, vous devriez voir que l'eau est liquide tout de même"
 *             << std::endl;
 * }
 * \endcode
 */
void saisir_valeur(const char *message, bool &sortie, char yes, char no);

/*!\brief Fonction de simplification de la fonction saisir_valeur()
 * spécialisée au type \p bool utilisable dans une expression
 *
 * Cette fonction n'est qu'une coquille appelant la fonction
 * saisir_valeur() avec des paramètres spécialisés, notamment le type
 * \p bool de la variable de sortie. Elle a cependant l'avantage de
 * retourner une valeur booléenne et peut donc être utilisée
 * directement dans un test.
 *
 * \param message À afficher à l'utilisateur pour qu'il comprenne ce qu'on attend de lui
 * \param yes Si fourni, le caractère à afficher comme réponse positive à la question, sinon \p 'o' par défaut.
 * \param no Si fourni, le caractère à afficher comme réponse négative à la question, sinon \p 'n' par défaut.
 *
 * \return Valeur booléenne \p bool correspondant à ce que
 * l'utilisateur aura saisi. \p true si l'utilisateur a saisi le
 * caractère correspondant à \p yes, ou bien \p false si l'utilisateur
 * a saisi le caractère correspondant à \p no.
 *
 * \par Exemples
 * \li Poser une question binaire à l'utilisateur et obtenir une réponse basée sur le choix entre \p 'o' et \p 'n' (Oui/Non).
 * \code{.cpp}
 * if(saisie_oui_non("Avez-vous pris une tasse de thé aujourd'hui ?") == true){
 *   std::cout << "Vous venez de déclarer que vous aviez pris une tasse de thé aujourd'hui"
 *             << std::endl;
 * }
 * else{
 *   std::cout << "Vous venez de déclarer que vous n'aviez pas pris une tasse de thé aujourd'hui"
 *             << std::endl;
 * }
 * \endcode
 * \li Poser une question binaire à l'utilisateur et obtenir une réponse basée sur le choix entre \p 'V' et \p 'F' (Vrai/Faux).
 * \code{.cpp}
 * if(saisie_oui_non("À 20°C, l'eau est à l'état liquide ?", 'V', 'F') == true){
 *   std::cout << "Bravo, vous avez correctement répondu"
 *             << std::endl;
 * }
 * else{
 *   std::cout << "Oh ! Regardez autour de vous, vous devriez voir que l'eau est liquide tout de même"
 *             << std::endl;
 * }
 * \endcode
 */
bool saisie_oui_non(const char *message, char yes, char no);

/*!\brief Implémentation de la spécialisation \p std::string du patron saisir_valeur()
 *
 * Utilisez cette fonction lorsque vous souhaitez donner la
 * possibilité à l'utilisateur de saisir une chaîne de caractères qui
 * sera stockée dans une variable de type \p std::string
 *
 * \param message À afficher à l'utilisateur pour qu'il comprenne ce qu'on attend de lui
 * \param[out] sortie La variable en sortie, de type \p std::string, pour accueillir ce que l'utilisateur aura saisi
 *
 * \par Exemple
 * \code{.cpp}
 * std::string reponse_libre;
 * do{
 *   saisir_valeur("Donnez un commentaire sur la qualité de notre échange", &reponse_libre);
 * } while(saisie_oui_non("Validez-vous ce commentaire ?") == false);
 * std::cout << "Vous avez validé le commentaire suivant concernant la qualité de notre échange :"
 *           << std::endl
 *           << reponse_libre
 *           << std::endl;
 * \endcode
 */
void saisir_valeur(const char *message, std::string &sortie);


/// \cond
void saisir_valeur(const char *, char &, char, char);
/// \endcond
}
#endif
