#include "libsio.h"
#include <iostream>

#if HAVE_CXX11
  #include <random>
#else
  #include <cstdlib>
  #include <ctime>
#endif

#include <cctype>
#include <limits>

namespace sio {

unsigned int aleatoire(unsigned int debut, unsigned int fin)
{
  if(debut == fin)
  {
    return debut;
  }

  unsigned int valeur_aleatoire = 0;
#if HAVE_CXX11
  std::random_device graine;
  std::mt19937 generateur(graine());
#else
  std::srand(std::time(0));
#endif

  if(debut > fin)
  {
    unsigned int swap = debut;
    debut = fin;
    fin = swap;
  }

#if HAVE_CXX11
  std::uniform_int_distribution<unsigned int> distribution(debut, fin);
  valeur_aleatoire = distribution(generateur);
#else
  valeur_aleatoire = (std::rand() % (fin - debut)) + debut;
#endif
  return valeur_aleatoire;
}


template<typename T>
void saisir_valeur(const char *message,
                   T &sortie,
                   T _min,
                   T _max)
{
  bool again = false;

  do
  {
    std::cout << message << " [" << _min << ".." << _max << "] : ";

    if(std::cin >> sortie)
    {
      if(sortie < _min || sortie > _max)
      {
        again = true;
      }
      else
      {
        again = false;
      }
    }
    else
    {
      again = true;
    }

    if(again)
    {
      std::cout << "\t";
    }

    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  } while(again);
}


void saisir_valeur(const char *message,
                   char &sortie,
                   char minimum = 32,
                   char maximum = 126)
{
  do
  {
    std::cout << message << " ['" << minimum << "'..'" << maximum << "'] : ";
    std::cin.get(sortie);

    if(sortie != '\n')
    {
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    if(sortie < minimum || sortie > maximum)
    {
      std::cout << "\t";
    }
  } while(sortie < minimum || sortie > maximum);
}


void saisir_valeur(const char *message,
                   std::string &sortie)
{
  bool again = false;
  std::cin.clear();

  do
  {
    std::cout << message << " [jusqu'à " << sortie.max_size() << " caractères] : ";
    std::getline(std::cin, sortie);

    if(sortie.empty())
    {
      again = true;
      std::cout << "\t";
    }
    else
    {
      again = false;
    }
  } while(again);
}


void saisir_valeur(const char *message,
                   bool &sortie,
                   char yes = 'o',
                   char no = 'n')
{
  char input = '\0';

  while(std::tolower(input) != yes
        && std::tolower(input) != no)
  {
    std::cout << message << " [" << yes << "/" << no << "] : ";
    std::cin.get(input);

    if(input != '\n')
    {
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
  }

  sortie = (std::tolower(input) == yes);
}


template<typename T>
void saisir_valeur(const char *message, T &sortie)
{
  sio::saisir_valeur(message,
                     sortie,
                     std::numeric_limits<T>::lowest(),
                     std::numeric_limits<T>::max());
}


bool saisie_oui_non(const char *message,
                    char yes = 'o',
                    char no = 'n')
{
  bool retour;
  saisir_valeur(message, retour, yes, no);
  return retour;
}

/*
 Cette partie est utile uniquement pour permettre d'obtenir dans la
 libsio.so le code objet correspondant qui, puisqu'il n'est pas
 utilisé dans une quelconque partie du code, ne serait pas conçu par
 le compilateur.
*/
template void saisir_valeur<char>(const char *message,
                                  char &sortie);
template void saisir_valeur<short int>(const char *,
                                       short int &);
template void saisir_valeur<short int>(const char *,
                                       short int &,
                                       short int,
                                       short int);
template void saisir_valeur<int>(const char *,
                                 int &);
template void saisir_valeur<int>(const char *,
                                 int &,
                                 int,
                                 int);
template void saisir_valeur<long int>(const char *,
                                      long int &);
template void saisir_valeur<long int>(const char *,
                                      long int &,
                                      long int,
                                      long int);
template void saisir_valeur<float>(const char *,
                                   float &);
template void saisir_valeur<float>(const char *,
                                   float &,
                                   float,
                                   float);
template void saisir_valeur<double>(const char *,
                                    double &);
template void saisir_valeur<double>(const char *,
                                    double &,
                                    double,
                                    double);
}
